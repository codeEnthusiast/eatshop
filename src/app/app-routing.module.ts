import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { RecipiesComponent } from './recipies/recipies.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { RecipieDetailComponent } from './recipies/recipie-detail/recipie-detail.component';
import { RecipieEditComponent } from './recipies/recipie-edit/recipie-edit.component';
import { ShoppingEditComponent } from './shopping-list/shopping-edit/shopping-edit.component';


const appRoutes: Routes = [
  {path:'', redirectTo: 'recipes', pathMatch:'full'},
  {path:'recipes', component: RecipiesComponent, children:
  [
    {path:'new', component: RecipieEditComponent},
    {path:':id', component: RecipieDetailComponent},
    {path:':id/edit', component: RecipieEditComponent}
  ] },
  {path:'shopping-list', component:ShoppingListComponent,children:[
    {path:':id/edit', component: ShoppingEditComponent}
  ] } 
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes)
  ],
  declarations: [],
  exports:[
    RouterModule
  ]
})
export class AppRoutingModule { }
