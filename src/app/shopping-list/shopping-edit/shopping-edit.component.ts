import { Component, OnInit, ViewChild } from '@angular/core';
import { Ingredient } from 'src/app/shared/ingredient.model';
import { ShoppingListService } from 'src/app/shared/services/shopping-list.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {

  selectedIngredientIndex: number;
  editMode=false;
  @ViewChild('shopEditForm') editForm;
  constructor( private shoppingListService: ShoppingListService) { }

  ngOnInit() 
  {
    this.shoppingListService.ingredientSelected.subscribe(
      (ingredient)=>{
        this.editMode=true;
        this.selectedIngredientIndex =this.shoppingListService.getIndexOfIngredient(ingredient);
        this.editForm.setValue(
          {
            name:ingredient.name,
            amt: ingredient.amount
          }
        )
      }
    )
  }

  addOrUpdateIngredients(form: NgForm)
  {
    if( !this.editMode)
    {
      this.shoppingListService.addIngredients(
        form.value.name, form.value.amt);
      this.editForm.reset();
    }
        
    else
    {
      console.log("Edit mode is:"+this.editMode);
      this.shoppingListService.updateIngredients(
        new Ingredient( form.value.name, form.value.amt));
      this.editMode=false;
      this.editForm.reset();
    }
        
  }

  onDelete( form: NgForm)
  {
    this.shoppingListService.deleteIngredient( 
                      new Ingredient( form.value.name, form.value.amt));
    this.editMode=false;
    this.editForm.reset();
  }

  onClear(form: NgForm)
  {
    this.editForm.reset();
    this.editMode=false;
  }
}
