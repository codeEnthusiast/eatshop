import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shared/services/shopping-list.service';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit 
{

  ingredients: Ingredient[]=[];


  constructor( private shoppingListService: ShoppingListService) { }
    

  ngOnInit() 
  {
    this.ingredients = this.shoppingListService.getIngredientsList();
    console.log("Executes only once");
    this.shoppingListService.ingredientsChanged.subscribe(
      (ingredients: Ingredient[])=>{
       this.ingredients=ingredients;
      }
    );
  }

  onSelectShoppingListItem(ingredient: Ingredient)
  {
    console.log('onSelectShoppingListItem invoked');
    this.shoppingListService.ingredientSelected.next(ingredient);
  }
  
}
