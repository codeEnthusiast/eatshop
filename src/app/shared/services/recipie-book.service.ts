import { Injectable, EventEmitter } from '@angular/core';

import { Recipie} from '../../recipies/recipie.model';
import { Ingredient } from '../ingredient.model';

@Injectable({
  providedIn: 'root'
})
export class RecipieBookService {

  recipies: Recipie[] =[
    new Recipie( 1,
                'Complete Meal', 
                'South-Indian Thali', 
                'https://cdn.pixabay.com/photo/2017/09/09/12/09/india-2731812_960_720.jpg',
                [new Ingredient('Rotis',2), new Ingredient('Dal',1), new Ingredient('Rice',1)]),
    new Recipie( 2, 
                'Puri', 
                'Puri with Chole', 
                'https://cdn.pixabay.com/photo/2016/11/23/18/31/indian-food-1854247_960_720.jpg',
                [new Ingredient('Wheat',2), new Ingredient('Chick Peas',5)]),
    new Recipie( 3,
                'Idly',
                 'Idly with Sambar, hutney', 
                 'https://cdn.pixabay.com/photo/2017/06/16/11/38/breakfast-2408818_960_720.jpg',
                [new Ingredient('Urad Dal',2), new Ingredient('Vegetables',4)]),
  ];

  recipieSelectEvent = new EventEmitter<Recipie>();
  constructor() { }

  getRecipies()
  {
    return this.recipies.slice();
  }

  getRecipieDetails(id: number): Recipie
  {
    return this.recipies[--id];
  }


}
