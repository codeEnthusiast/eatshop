import { Injectable,EventEmitter } from '@angular/core';
import { Subject} from 'rxjs';
import{ Ingredient} from '../ingredient.model';

@Injectable({
  providedIn: 'root'
})
export class ShoppingListService {

private ingredients = [
    new Ingredient( "Apple",5),
    new Ingredient("Tomatoes",10)
  ];

  selectedIngredient: Ingredient;
  ingredientSelected = new Subject<Ingredient>();
  ingredientsChanged = new Subject<Ingredient[]>();

  constructor() { }

  
  getIngredientsList()
  {
    return this.ingredients.slice();
  }

  getIndexOfIngredient(ingredient: Ingredient): number
  {
    return this.ingredients.indexOf(ingredient);
  }

  addIngredients( name: string, amt: number)
  {
    //this.ingredientAddEvent.emit(new Ingredient(name, amt));
    this.ingredients.push( new Ingredient(name,amt));
    this.ingredientsChanged.next(this.ingredients.slice());
  }

  updateIngredients( ingredient: Ingredient)
  {
    let index=this.ingredients.findIndex((i)=>i.name===ingredient.name);
    this.ingredients[index]=ingredient;
    this.ingredientsChanged.next(this.ingredients.slice());
  }

  deleteIngredient( ingredient: Ingredient)
  {
    let index = this.ingredients.findIndex((i)=>i.name===ingredient.name);
    this.ingredients.splice(index,1);
    console.log(this.ingredients);
    this.ingredientsChanged.next(this.ingredients.slice());

  }
 
}
