import { Directive, HostListener, HostBinding, ElementRef } from '@angular/core';

@Directive({
  selector: '[appDropDown]'
})
export class DropDownDirective {

  @HostBinding('class.open') appDropDown=false;

  constructor() { }

  @HostListener('click') onclick()
  {
    console.log("onclick called");
    if( this.appDropDown)
      this.appDropDown=false;
    else
      this.appDropDown=true;
  }


  
}
