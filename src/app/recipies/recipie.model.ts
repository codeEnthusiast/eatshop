import { Ingredient } from "../shared/ingredient.model";

export class Recipie
{
    recipieId:number;
    name: string;
    description: string;
    imagePath: string;
    ingredients: Ingredient[];

    constructor( recipieId:number,
                 name: string, 
                 desc: string, 
                 imagePath: string,
                 ingredients: Ingredient[])
    {
        this.recipieId=recipieId;
        this.name= name;
        this.description = desc;
        this.imagePath=imagePath;
        this.ingredients=ingredients;
    }
}