import { Component, OnInit, Input } from '@angular/core';
import { Recipie } from '../recipie.model';
import { ActivatedRoute } from '@angular/router';
import { RecipieBookService } from 'src/app/shared/services/recipie-book.service';

@Component({
  selector: 'app-recipie-detail',
  templateUrl: './recipie-detail.component.html',
  styleUrls: ['./recipie-detail.component.css']
})
export class RecipieDetailComponent implements OnInit {

  recipe:Recipie;
  constructor( private aRoute:ActivatedRoute,
                private recipieService:RecipieBookService) { }

  ngOnInit() {
    console.log("RecipeDetailComponent onInit called");
    this.aRoute.params.subscribe(
      (params)=>{
        this.recipe = this.recipieService.getRecipieDetails(+params['id']);
      }
    );
  }

}
