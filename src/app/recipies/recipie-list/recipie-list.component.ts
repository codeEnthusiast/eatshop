import { Component, OnInit } from '@angular/core';

import{ Recipie} from '../recipie.model';
import { RecipieBookService } from 'src/app/shared/services/recipie-book.service';


@Component({
  selector: 'app-recipie-list',
  templateUrl: './recipie-list.component.html',
  styleUrls: ['./recipie-list.component.css']
})
export class RecipieListComponent implements OnInit {

  
  recipies: Recipie[]=[];
 
  constructor( private recipieService: RecipieBookService) { 
    
  }

  ngOnInit() {
    this.recipies=this.recipieService.getRecipies();
  }

}
