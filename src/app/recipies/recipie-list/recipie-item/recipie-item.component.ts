import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { Recipie } from '../../recipie.model';
import { RecipieBookService } from 'src/app/shared/services/recipie-book.service';

@Component({
  selector: 'app-recipie-item',
  templateUrl: './recipie-item.component.html',
  styleUrls: ['./recipie-item.component.css']
})
export class RecipieItemComponent implements OnInit {

  @Input() recipe: Recipie;
  //recipeToDisplay:Recipie;
  constructor( private recipieService: RecipieBookService) { }

  ngOnInit() {
  }
}
