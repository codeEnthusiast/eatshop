import { Component, OnInit } from '@angular/core';
import { Recipie } from './recipie.model';
import { RecipieBookService } from '../shared/services/recipie-book.service';

@Component({
  selector: 'app-recipies',
  templateUrl: './recipies.component.html',
  styleUrls: ['./recipies.component.css']
})
export class RecipiesComponent implements OnInit {

  constructor( private recipieService: RecipieBookService) { }
  

  ngOnInit() {
    
  }

}
