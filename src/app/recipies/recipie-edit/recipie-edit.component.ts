import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-recipie-edit',
  templateUrl: './recipie-edit.component.html',
  styleUrls: ['./recipie-edit.component.css']
})
export class RecipieEditComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }
  editRecipe:boolean =false;

  ngOnInit() {
    this.route.params.subscribe(
      (params) => {
          this.editRecipe = params['id']!=null;
         
      }
    );
  }

}
