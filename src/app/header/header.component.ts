import { Component, EventEmitter, Output } from "@angular/core";
import{ DropDownDirective} from '../shared/drop-down.directive';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
})
export class Header
{

    @Output() userSelectEvent = new EventEmitter();
    userSelection="";

    
}